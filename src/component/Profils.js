import React, { Component } from 'react';
import "../css/profils.css";
import SidBarProfil from './SidBarProfil';


function Profils({ image, title, description, postedOn }) {

    return (
        <div>
            <div className="profil-container">

            
            <div className="profil-bloc">
                <div className="image-comtainer">

                    <img src={image} alt="profil ilage"  />
                </div>
                <h1 className="profil-title">{title}</h1>
                <span>Posted on: {postedOn}</span>
                <p className="profil-desci">{description}</p>
            </div>


            </div>

        </div>

    )

}

export default Profils;