import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/souNavbar.css';




class SouNav extends Component {

    state = {
        search: false,
    }

    showSearch = () => {
        this.setState({
            search: !this.state.search
        });
    }



    searchSubmit = (event) => {
        event.preventDefault();
        alert("search");
    }

    render() {
        const styleDecoratio = {
            textDecoration: 'none',
            color: 'whitesmoke'
        }





        return (
            <div className="sous-navebar">

                <div className="souNav">
                    <ul className="linkss"  >
                        <Link to="/" style={styleDecoratio}>
                            <li style={styleDecoratio}>Home</li>
                        </Link>
                        <Link to="/post/" style={styleDecoratio}>
                            <li style={styleDecoratio}>Posts</li>
                        </Link>
                         <Link to="/about-us" style={styleDecoratio}>
                            <li style={styleDecoratio}>About-Us</li>
                        </Link>

                    </ul>
                </div>

                <div className="search">
                    <form onSubmit={this.searchSubmit}>
                        {this.state.search &&

                            <input type="text" placeholder="search" className="searchInput" id="input" />
                        }
                       

                        <i onClick={this.showSearch} className="serachIcon fas fa-search"  id="serach"></i>


                    </form>

                </div>





            </div>
        )
    }
}

export default SouNav;